package de.sideverse.owe;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

import static org.bukkit.Bukkit.*;

public class GetElytra implements Listener {

    ItemStack elytra;
    ItemMeta meta;
    static int heightLimit;

    public GetElytra(){
        elytra = new ItemStack(Material.ELYTRA);
        elytra.addEnchantment(Enchantment.BINDING_CURSE, 1);
        elytra.addEnchantment(Enchantment.VANISHING_CURSE, 1);

        meta = elytra.getItemMeta();
        meta.setUnbreakable(true);
        meta.setDisplayName(OWE.elytraname);
        elytra.setItemMeta(meta);

        if(Objects.equals(OWE.height, "auto")){
            heightLimit = (getServer().getWorlds().get(0).getSpawnLocation().getBlockY() - 5);
        }else{
            try {
                heightLimit = Integer.parseInt(OWE.height);
            }catch (NumberFormatException e){
                e.printStackTrace();
                heightLimit = (getServer().getWorlds().get(0).getSpawnLocation().getBlockY() - 5);
                getLogger().warning(OWE.height + " is not a number! Height was is set to " + heightLimit + "!");
            }
        }
    }

    public void giveElytra(Player player){
        if(player.getBedSpawnLocation() == null) {
            //boolean advancement = player.getAdvancementProgress(getServer().getAdvancement(NamespacedKey.minecraft("end/elytra"))).isDone());
            player.getInventory().setChestplate(elytra);
            //if(!advancement){
            //    player.getAdvancementProgress(getServer().getAdvancement(NamespacedKey.minecraft("end/elytra"))).
            //}
            player.sendMessage(OWE.message);

        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event){
        Player player = event.getPlayer();
        if(player.getInventory().isEmpty()) {
            giveElytra(event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        if(!player.hasPlayedBefore()){
            giveElytra(player);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();

        if(!player.isGliding() && player.getInventory().getChestplate() != null && Objects.requireNonNull(player.getInventory().getChestplate().getItemMeta()).isUnbreakable() && player.getLocation().getBlockY() < heightLimit){
            player.getInventory().setChestplate(null);
        }
    }
}
