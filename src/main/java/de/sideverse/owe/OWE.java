package de.sideverse.owe;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public final class OWE extends JavaPlugin {

    public static String message;
    public static String elytraname;
    public static String height;

    public void propertiesLoader() throws IOException {
        if(!(new File("./plugins/OWE/OWE.conf")).exists()) {
            File config = new File("./plugins/OWE/OWE.conf");

            File path = new File("./plugins/OWE");
            if(!(path.exists())) {
                if(!path.mkdirs()) {
                    getLogger().warning("No config directory or file could be created");
                }else{
                    getLogger().info("Config directory and file were successfully created");
                }
            }

            FileWriter writer = new FileWriter(config);
            writer.write("# Config File of OWE\n\nHeight=auto\nMessage=§2Elytra equipped!\nElytraName=One Way Elytra");
            writer.close();
        }
        try (FileReader reader = new FileReader("./plugins/OWE/OWE.conf")) {
            Properties properties = new Properties();
            properties.load(reader);

            message = properties.getProperty("Message");
            if(message == null){
                message = "§2Elytra equipped!";
            }

            elytraname = properties.getProperty("ElytraName");
            if(elytraname == null){
                elytraname = "One Way Elytra";
            }

            height = properties.getProperty("Height");
            if(height == null){
                height = "auto";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        try {
            propertiesLoader();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getServer().getPluginManager().registerEvents(new GetElytra(), this);
        this.getLogger().info("Succesfully started!");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        this.getLogger().info("Succesfully stopped!");
    }
}
