# OWE

One-Way-Elytra Plugin for Minecraft on Respawn

Gives the player an elytra on respawn after death which vanishes, if the player hits the ground at least 5 blocks under the world spawn.

**This Plugin only supports Linux and Mac yet! This should be fine with most server hosters like Nitrado, etc. but if your server runs on your own Windows PC, you can't configure this plugin.**